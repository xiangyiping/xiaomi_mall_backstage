package com.xyp.yipingshop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xyp.yipingshop.pojo.ProductNav;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
public interface ProductNavMapper extends BaseMapper<ProductNav> {

}
