package com.xyp.yipingshop.mapper;

import com.xyp.yipingshop.pojo.ProductProperty;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
public interface ProductPropertyMapper extends BaseMapper<ProductProperty> {

}
