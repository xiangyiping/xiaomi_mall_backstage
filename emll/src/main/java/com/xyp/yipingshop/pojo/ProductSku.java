package com.xyp.yipingshop.pojo;

import java.math.BigDecimal;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;


/**
 * <p>
 * 
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@TableName("product_sku")
public class ProductSku implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * sku(库存量单位)编码
     */
    @TableId(value = "sku_id", type = IdType.AUTO)
    private Integer skuId;
    /**
     * sku名称
     */
    @TableField("sku_name")
    private String skuName;
    /**
     * sku编码
     */
    @TableField("sku_code")
    private String skuCode;
    /**
     * sku价格
     */
    @TableField("sku_price")
    private BigDecimal skuPrice;
    /**
     * spu(标准化产品单元)id
     */
    @TableField("spu_id")
    private Integer spuId;


    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public BigDecimal getSkuPrice() {
        return skuPrice;
    }

    public void setSkuPrice(BigDecimal skuPrice) {
        this.skuPrice = skuPrice;
    }

    public Integer getSpuId() {
        return spuId;
    }

    public void setSpuId(Integer spuId) {
        this.spuId = spuId;
    }

    @Override
    public String toString() {
        return "ProductSku{" +
        ", skuId=" + skuId +
        ", skuName=" + skuName +
        ", skuCode=" + skuCode +
        ", skuPrice=" + skuPrice +
        ", spuId=" + spuId +
        "}";
    }
}
