package com.xyp.yipingshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@TableName("product_property")
public class ProductProperty implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 属性id
     */
    @TableId("product_property_id")
    private Integer productPropertyId;
    /**
     * 属性名称
     */
    @TableField("product_property_name")
    private String productPropertyName;
    /**
     * 分类id
     */
    @TableField("product_property_type_id")
    private Integer productPropertyTypeId;


    public Integer getProductPropertyId() {
        return productPropertyId;
    }

    public void setProductPropertyId(Integer productPropertyId) {
        this.productPropertyId = productPropertyId;
    }

    public String getProductPropertyName() {
        return productPropertyName;
    }

    public void setProductPropertyName(String productPropertyName) {
        this.productPropertyName = productPropertyName;
    }

    public Integer getProductPropertyTypeId() {
        return productPropertyTypeId;
    }

    public void setProductPropertyTypeId(Integer productPropertyTypeId) {
        this.productPropertyTypeId = productPropertyTypeId;
    }

    @Override
    public String toString() {
        return "ProductProperty{" +
        ", productPropertyId=" + productPropertyId +
        ", productPropertyName=" + productPropertyName +
        ", productPropertyTypeId=" + productPropertyTypeId +
        "}";
    }
}
