package com.xyp.yipingshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@TableName("product_nav")
public class ProductNav implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 导航id
     */
    @TableId("product_nav_id")
    private Integer productNavId;
    /**
     * 导航名称
     */
    @TableField("product_nav_name")
    private String productNavName;


    public Integer getProductNavId() {
        return productNavId;
    }

    public void setProductNavId(Integer productNavId) {
        this.productNavId = productNavId;
    }

    public String getProductNavName() {
        return productNavName;
    }

    public void setProductNavName(String productNavName) {
        this.productNavName = productNavName;
    }

    @Override
    public String toString() {
        return "ProductNav{" +
        ", productNavId=" + productNavId +
        ", productNavName=" + productNavName +
        "}";
    }
}
