package com.xyp.yipingshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@TableName("product_nav_spu")
public class ProductNavSpu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * spuid
     */
    @TableId("product_spu_id")
    private Integer productSpuId;
    /**
     * navid
     */
    @TableField("product_nav_id")
    private Integer productNavId;


    public Integer getProductSpuId() {
        return productSpuId;
    }

    public void setProductSpuId(Integer productSpuId) {
        this.productSpuId = productSpuId;
    }

    public Integer getProductNavId() {
        return productNavId;
    }

    public void setProductNavId(Integer productNavId) {
        this.productNavId = productNavId;
    }

    @Override
    public String toString() {
        return "ProductNavSpu{" +
        ", productSpuId=" + productSpuId +
        ", productNavId=" + productNavId +
        "}";
    }
}
