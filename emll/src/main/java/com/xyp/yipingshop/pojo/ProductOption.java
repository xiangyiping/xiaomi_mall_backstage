package com.xyp.yipingshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@TableName("product_option")
public class ProductOption implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 属性选项id
     */
    @TableId("product_property_option_id")
    private Integer productPropertyOptionId;
    /**
     * 选项值
     */
    @TableField("product_option_value")
    private String productOptionValue;
    /**
     * 属性id
     */
    @TableField("product_property_id")
    private Integer productPropertyId;


    public Integer getProductPropertyOptionId() {
        return productPropertyOptionId;
    }

    public void setProductPropertyOptionId(Integer productPropertyOptionId) {
        this.productPropertyOptionId = productPropertyOptionId;
    }

    public String getProductOptionValue() {
        return productOptionValue;
    }

    public void setProductOptionValue(String productOptionValue) {
        this.productOptionValue = productOptionValue;
    }

    public Integer getProductPropertyId() {
        return productPropertyId;
    }

    public void setProductPropertyId(Integer productPropertyId) {
        this.productPropertyId = productPropertyId;
    }

    @Override
    public String toString() {
        return "ProductOption{" +
        ", productPropertyOptionId=" + productPropertyOptionId +
        ", productOptionValue=" + productOptionValue +
        ", productPropertyId=" + productPropertyId +
        "}";
    }
}
