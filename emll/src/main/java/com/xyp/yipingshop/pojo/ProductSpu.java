package com.xyp.yipingshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@TableName("product_spu")
public class ProductSpu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * spu_id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;
    /**
     * spu编码
     */
    @TableField("product_synopsis")
    private String productSynopsis;
    /**
     * 简介
     */
    @TableField("product_detail")
    private String productDetail;
    /**
     * 分类id
     */
    @TableField("product_type_id")
    private String productTypeId;
    /**
     * 品牌id
     */
    @TableField("product_brand_id")
    private Integer productBrandId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSynopsis() {
        return productSynopsis;
    }

    public void setProductSynopsis(String productSynopsis) {
        this.productSynopsis = productSynopsis;
    }

    public String getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    public String getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(String productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getProductBrandId() {
        return productBrandId;
    }

    public void setProductBrandId(Integer productBrandId) {
        this.productBrandId = productBrandId;
    }

    @Override
    public String toString() {
        return "ProductSpu{" +
        ", id=" + id +
        ", productName=" + productName +
        ", productSynopsis=" + productSynopsis +
        ", productDetail=" + productDetail +
        ", productTypeId=" + productTypeId +
        ", productBrandId=" + productBrandId +
        "}";
    }
}
