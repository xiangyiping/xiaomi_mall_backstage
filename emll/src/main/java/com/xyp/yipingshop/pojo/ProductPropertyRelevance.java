package com.xyp.yipingshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@TableName("product_property_relevance")
public class ProductPropertyRelevance implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * sku属性选项关联
     */
    @TableId(value = "product_property_id", type = IdType.AUTO)
    private Integer productPropertyId;
    /**
     * product_sku_id
     */
    @TableField("product_sku_id")
    private Integer productSkuId;
    /**
     * 属性选项id
     */
    @TableField("product_property_opion_id")
    private Integer productPropertyOpionId;


    public Integer getProductPropertyId() {
        return productPropertyId;
    }

    public void setProductPropertyId(Integer productPropertyId) {
        this.productPropertyId = productPropertyId;
    }

    public Integer getProductSkuId() {
        return productSkuId;
    }

    public void setProductSkuId(Integer productSkuId) {
        this.productSkuId = productSkuId;
    }

    public Integer getProductPropertyOpionId() {
        return productPropertyOpionId;
    }

    public void setProductPropertyOpionId(Integer productPropertyOpionId) {
        this.productPropertyOpionId = productPropertyOpionId;
    }

    @Override
    public String toString() {
        return "ProductPropertyRelevance{" +
        ", productPropertyId=" + productPropertyId +
        ", productSkuId=" + productSkuId +
        ", productPropertyOpionId=" + productPropertyOpionId +
        "}";
    }
}
