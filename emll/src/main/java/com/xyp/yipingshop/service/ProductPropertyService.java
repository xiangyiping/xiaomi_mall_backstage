package com.xyp.yipingshop.service;

import com.xyp.yipingshop.pojo.ProductProperty;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
public interface ProductPropertyService extends IService<ProductProperty> {

}
