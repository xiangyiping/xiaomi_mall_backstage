package com.xyp.yipingshop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xyp.yipingshop.pojo.ProductNavSpu;
import com.xyp.yipingshop.uitl.ResponseResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
public interface ProductNavSpuService extends IService<ProductNavSpu> {
    public ResponseResult<ProductNavSpu> findall();
}
