package com.xyp.yipingshop.service;

import com.xyp.yipingshop.pojo.ProductSku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
public interface ProductSkuService extends IService<ProductSku> {

}
