package com.xyp.yipingshop.service;

import com.xyp.yipingshop.pojo.ProductNav;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xyp.yipingshop.uitl.ResponseResult;

/**
 * <p>
 *  导航服务类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
public interface ProductNavService extends IService<ProductNav> {
    /**
     * 添加导航标签
     * @param productNav
     * @return
     */
    Integer inserProductNav(ProductNav productNav);

    /**
     * 通过导航Id删除导航标签
     * @param productNavId
     * @return
     */
    Integer delProductNavById(Integer productNavId);

    /**
     * 通过ProductNav修改ProductNav
     * @param productNav
     * @return
     */
    Integer updateProductNav(ProductNav productNav);
    /**
     * 查询导航标签
     * @return
     */
    ResponseResult<ProductNav> selectProductNav();


}
