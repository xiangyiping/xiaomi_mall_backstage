package com.xyp.yipingshop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xyp.yipingshop.pojo.ProductBrand;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
public interface ProductBrandService extends IService<ProductBrand> {

}
