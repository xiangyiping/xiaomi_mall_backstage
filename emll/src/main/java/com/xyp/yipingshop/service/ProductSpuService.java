package com.xyp.yipingshop.service;

import com.xyp.yipingshop.pojo.ProductSpu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xyp.yipingshop.uitl.ResponseResult;

/**
 * <p>
 *  产品标准单位服务类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
public interface ProductSpuService extends IService<ProductSpu> {

    /**
     * 新增产品
     * @param productSpu
     * @return
     */
    Integer insertProductSpu(ProductSpu productSpu);

    /**
     * 通过 productSpu修改
     * @param productSpu
     * @return
     */
    Integer updateProductSpuByproductSpu(ProductSpu productSpu);

    /**
     * 通过spuid删除productSpu
     * @param spuid
     * @return
     */
    Integer delProductSpuById(Integer spuid);

    ResponseResult<ProductSpu> selectProduct();

}
