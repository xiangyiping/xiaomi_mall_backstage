package com.xyp.yipingshop.controller;


import com.xyp.yipingshop.pojo.ProductSpu;
import com.xyp.yipingshop.service.ProductSpuService;
import com.xyp.yipingshop.uitl.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@RestController
@RequestMapping("/productSpu")
public class ProductSpuController {

    @Autowired
    private ProductSpuService productSpuService;


    /**
     * 新增产品
     * @param productSpu
     * @return
     */
    @PostMapping("/insertProductSpu")
    public Integer insertProductSpu(@RequestBody  ProductSpu productSpu) {
        return productSpuService.insertProductSpu(productSpu);
    }
    /**
     * 通过 productSpu修改
     * @param productSpu
     * @return
     */
    @PostMapping("/updateProductSpuByproductSpu")
    public Integer updateProductSpuByproductSpu(ProductSpu productSpu) {
        return productSpuService.updateProductSpuByproductSpu(productSpu);
    }
    /**
     * 通过spuid删除productSpu
     * @param spuid
     * @return
     */
    @RequestMapping("/delProductSpuByid")
    public Integer delProductSpuById(Integer spuid) {
        return productSpuService.delProductSpuById(spuid);
    }

    /**
     * 查询ProductSpu
     * @return
     */
    @RequestMapping("/selectProductSpu")
    public ResponseResult<ProductSpu> selectProduct() {
        return productSpuService.selectProduct();
    }
}

