package com.xyp.yipingshop.controller;


import com.xyp.yipingshop.pojo.ProductNav;
import com.xyp.yipingshop.service.ProductNavService;
import com.xyp.yipingshop.uitl.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@RestController
@RequestMapping("/productNav")
public class ProductNavController {
    @Autowired
    private ProductNavService productNavService;
    /**
     * 通过导航新增
     * @return 1:成功 outher:失败
     */
    @RequestMapping("/insert")
    public Integer inserProductNav(){
        ProductNav productNav=new ProductNav();
        productNav.setProductNavName("小米");
        return productNavService.inserProductNav(productNav);
    }

    /**
     * 通过Id删除导航
     * @return
     */
    @RequestMapping("delProductNavById")
    public Integer delProductNavByid(){
        return productNavService.delProductNavById(1);
    }

    /**
     * 通过ID修改导航
     * @return
     */
    @PostMapping("updateproductNavByid")
    public Integer updateProductNavByid(@RequestBody  ProductNav productNav){
        System.err.println(productNav.toString());
        return  productNavService.updateProductNav(productNav);
    }
    /**
     * 查询所有导航
     * @return
     */
    @RequestMapping("selectProductNav")
    public ResponseResult<ProductNav> selectProductNav(){
        return  productNavService.selectProductNav();
    }
}

