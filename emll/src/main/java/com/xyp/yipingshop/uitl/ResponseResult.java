package com.xyp.yipingshop.uitl;

import java.io.Serializable;
import java.util.List;

/**
 * 响应结果类型
 * @param <T> 服务器向客户端响应的数据的类型
 */
public class  ResponseResult<T> implements Serializable {

    private static final long serialVersionUID = -1626793180717240861L;
    /*
     * 当前状态码
     */
    private  Integer code;
    /*
     * 当前页面
     */
    private Integer cover;
    /*
     * 数据
     */
    private List<T> data;
    /*
     * 总条数
     */
    private Integer count;


    /*
     * 错误报告
     */
    private String msg;


    /**
     * @return the code
     */
    public Integer getCode() {
        return code;
    }


    /**
     * @param code the code to set
     */
    public void setCode(Integer code) {
        this.code = code;
    }


    /**
     * @return the cover
     */
    public Integer getCover() {
        return cover;
    }


    /**
     * @param cover the cover to set
     */
    public void setCover(Integer cover) {
        this.cover = cover;
    }


    /**
     * @return the data
     */
    public List<T> getData() {
        return data;
    }


    /**
     * @param data the data to set
     */
    public void setData(List<T> data) {
        this.data = data;
    }


    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }


    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }


    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }


    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }


    public ResponseResult() {
        super();
        // TODO Auto-generated constructor stub
    }
    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }


     public   ResponseResult(Integer code, Integer cover, List<T> data, Integer count, String msg) {
        super();
        this.code = code;
        this.cover = cover;
        this.data = data;
        this.count = count;
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ResponseResult{" +
                "code=" + code +
                ", cover=" + cover +
                ", data=" + data +
                ", count=" + count +
                ", msg='" + msg + '\'' +
                '}';
    }

    public static <T> ResponseResult  getResponseResult(Integer code, Integer cover, List<T> data, Integer count, String msg) {
        ResponseResult responseResult=new ResponseResult(code,cover,data,count,msg);
           return responseResult;
    }


}
