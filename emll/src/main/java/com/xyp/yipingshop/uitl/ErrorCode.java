package com.xyp.yipingshop.uitl;

/**
 * 错误码对应数据库error_code表的Key值
 * Created by Administrator on 2017/2/23 0023.
 */
public enum ErrorCode {
	/**
	 * 成功
	 */
	RESULT_SUCCESS("0","成功"),
	/**
	 * 保存失败
	 */
	SAVE_ERR("1","保存失败"),
	/**
	 * 尚未录入人脸
	 */
	DETECH_FIRST("3","尚未录入人脸"),
	/**static
	 * 添加失败"
	 */
	ADD_FAILED("4","添加失败"),

	/**
	 * 所传非图片文件
	 */
	NOT_IMAGE("5", "所传非图片文件"),
	/**
	 * 图片类型错误
	 */
	FILE_TYPE_ERROR_IMAGE("6", "图片类型错误"),
	/**
	 * 音频类型错误
	 */
	FILE_TYPE_ERROR_VIDEO("7", "音频类型错误"),
	/**
	 * 数据库错误
	 */
	DB_EXCEPTION("8","数据库错误"),
	/**
	 * 识别失败
	 */
	DETECH_FAILED("9","识别失败"),
	/**
	 * 参数错误
	 */
	ARGUMENT_ERR("10", "参数错误"),
	/**
	 * 参数不能为空
	 */
	ARGUMENT_IS_NULL("11", "参数不能为空"),
	/**
	 * 参数不能相同
	 */
	SAME_PARAMETERS("12","参数不能相同"),
	/**
	 * 上传文件为空
	 */
	FILE_NULL("13","上传文件为空"),
	/**
	 * 上传文件失败
	 */
	UPLOAD_FAILED("14","上传文件失败"),
	/**
	 * 文件不存在
	 */
	NO_FILE("15","文件不存在"),
	/**
	 * 已存在
	 */
	ALREADY_EXIST("16","已存在"),
	/**
	 * 不存在"
	 */
	NOT_EXIST("17","不存在"),
	/**
	 * 非法accesskey
	 */
	INVALID_ACCESS_KEY("18","非法accesskey"),
	/**
	 * 没有Authorization
	 */
	NEED_AUTHORD("19","没有Authorization"),
	/**
	 * 数据异常
	 */
	EXCEPTION_ERR("20","数据异常"),
	/**
	 * 人脸不止一个
	 */
	TOO_MUCH_FACE("21","人脸不止一个"),
	/**
	 * 无效sign
	 */
	INVALID_SIGN("22","无效sign"),
	/**
	 * 请求失败
	 */
	REQUEST_FAILED("23","请求失败"),
	/**
	 * Authorization格式不对
	 */
	INVALID_AUTHORIZATION_FORMAT("24","Authorization格式不对"),
	/**
	 * 手机号码格式错误
	 */
	PHONE_FORMAT_ERROR("25","手机号码格式错误"),
	
	/**
	 * 验证码不存在
	 */
	VERIFICATION_CODE_IS_EXIST("26","验证码不存在"),
	
	/**
	 * 验证码错误
	 */
	VERIFICATION_CODE_ERROR("27","验证码错误"),
	/**
	 * json错误
	 */
	JSON_ERR("28","json错误"),
	
	/**
	 * 商品价格错误
	 */
	GOODS_PRICE_ERROR("29","商品价格错误"),
	
	/**
	 * 已支付
	 */
	ALREADY_PAY("30","已支付"),
	/**
	 * 已支付
	 */
	PAY_FAILED("31","已支付"),
	
	/**
	 * 查询返回多个值
	 */
	FOUND("32","查询返回多个值"),
	
	/**
	 * 病人不存在
	 */
	SICK_EXIST("33","病人不存在"),
	
	/**
	 * 余额不足
	 */
	BALANCE_INSUFFICIENT("34","余额不足"),
	
	/**
	 * 病人已租用
	 */
	SICK_LEASED("35","病人已租用"),

	/**
	 * 扣费失败
	 */
	FAILURE_TO_DEDUCT_FEES("36","扣费失败"),

	/**
	 * 余额不足，无法退租
	 */
	NOT_SUFFICIENT_FUNDS("37","余额不足，无法退租"),

	/**
	 * 退租成功
	 */
	REFUND_SUCCESS("38","退租成功"),

	/**
	 * 退租失败
	 */
	REFUND_OF_RENT_FAILURE("39","退租失败"),

	/**
	 * 添加钱包失败
	 */
	ADD_WALLET_FAILED("40","添加钱包信息失败"),

	/**
	 * openid为空
	 */
	OPENID_CAN_T_FIND_IT("41","找不到该openid"),

	/**
	 * 用户已存在
	 */
	THE_USER_ALREADY_EXISTS("42","用户已存在"),

	/**
	 * 租金异常
	 */
	Rent_IS_ABNORMAL("43","租金异常"),

	/**
	 * 红包退还失败
	 */
	FAILED_TO_RETURN_RED_ENVELOPE("44","红包退还失败"),

	/**
	 * 退款已提交
	 */
	REFUND_SUBMITTED("45","退款已提交"),

	/**
	 * 退款中
	 */
	REFUND_IN_PROGRESS("46","退款中"),

	/**
	 * 退款成功
	 */
	REFUND_SUCCESSFUL("47","该订单退款成功"),

	/**
	 * 解密失败
	 */
	DECRYPTION_FAILED("48","解密失败"),

	/**
	 * 获取token失败
	 */
	GET_TOKEN_FAILED("49","获取token失败"),

	/**
	 * 插入订单错误
	 */
	INSERT_ORDER_ERROR("50","插入订单错误"),

	/**
	 * 用户不存在
	 */
	USER_DOES_NOT_EXIST("51","用户不存在"),

	/**
	 * 订单不存在
	 */
	ORDER_DOES_NOT_EXIST("52","订单不存在");


	private String code;
	private String message;


	ErrorCode(String code) {
		this.code = code;
	}

	ErrorCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
