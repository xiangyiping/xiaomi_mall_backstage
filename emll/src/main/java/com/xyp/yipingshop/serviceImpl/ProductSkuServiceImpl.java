package com.xyp.yipingshop.serviceImpl;

import com.xyp.yipingshop.pojo.ProductSku;
import com.xyp.yipingshop.mapper.ProductSkuMapper;
import com.xyp.yipingshop.service.ProductSkuService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductSkuServiceImpl extends ServiceImpl<ProductSkuMapper, ProductSku> implements ProductSkuService {

}
