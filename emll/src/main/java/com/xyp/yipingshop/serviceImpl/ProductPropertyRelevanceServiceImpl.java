package com.xyp.yipingshop.serviceImpl;

import com.xyp.yipingshop.pojo.ProductPropertyRelevance;
import com.xyp.yipingshop.mapper.ProductPropertyRelevanceMapper;
import com.xyp.yipingshop.service.ProductPropertyRelevanceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductPropertyRelevanceServiceImpl extends ServiceImpl<ProductPropertyRelevanceMapper, ProductPropertyRelevance> implements ProductPropertyRelevanceService {

}
