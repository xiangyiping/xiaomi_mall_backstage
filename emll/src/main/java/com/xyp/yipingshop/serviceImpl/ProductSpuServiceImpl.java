package com.xyp.yipingshop.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyp.yipingshop.pojo.ProductSpu;
import com.xyp.yipingshop.mapper.ProductSpuMapper;
import com.xyp.yipingshop.service.ProductSpuService;
import com.xyp.yipingshop.uitl.ResponseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 *  产品标准单位
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductSpuServiceImpl extends ServiceImpl<ProductSpuMapper, ProductSpu> implements ProductSpuService {

    @Resource
    private ProductSpuMapper productSpuMapper;

    @Override
    public Integer insertProductSpu(ProductSpu productSpu) {
        return productSpuMapper.insert(productSpu);
    }

    @Override
    public Integer updateProductSpuByproductSpu(ProductSpu productSpu) {
        return productSpuMapper.updateById(productSpu);
    }

    @Override
    public Integer delProductSpuById(Integer spuid) {
        return productSpuMapper.deleteById(spuid);
    }

    @Override
    public ResponseResult<ProductSpu> selectProduct() {
        return ResponseResult.getResponseResult(20,10,productSpuMapper.selectList(null),10,"success");
    }
}
