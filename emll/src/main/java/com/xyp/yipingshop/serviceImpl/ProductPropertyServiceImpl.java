package com.xyp.yipingshop.serviceImpl;

import com.xyp.yipingshop.pojo.ProductProperty;
import com.xyp.yipingshop.mapper.ProductPropertyMapper;
import com.xyp.yipingshop.service.ProductPropertyService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductPropertyServiceImpl extends ServiceImpl<ProductPropertyMapper, ProductProperty> implements ProductPropertyService {

}
