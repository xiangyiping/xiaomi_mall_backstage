package com.xyp.yipingshop.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyp.yipingshop.pojo.ProductNav;
import com.xyp.yipingshop.mapper.ProductNavMapper;
import com.xyp.yipingshop.service.ProductNavService;
import com.xyp.yipingshop.uitl.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  导航条服务实现类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductNavServiceImpl extends ServiceImpl<ProductNavMapper, ProductNav> implements ProductNavService {

    @Resource
    private ProductNavMapper productNavMapper;

    @Override
    public Integer inserProductNav(ProductNav productNav) {
        return productNavMapper.insert(productNav);
    }

    @Override
    public Integer delProductNavById(Integer productNavId) {
        return productNavMapper.deleteById(productNavId);
    }

    @Override
    public Integer updateProductNav(ProductNav productNav) {
        return productNavMapper.updateById(productNav);
    }

    @Override
    public ResponseResult<ProductNav> selectProductNav() {
        return ResponseResult.getResponseResult(200,10,productNavMapper.selectList(null),10,"success");
    }
}
