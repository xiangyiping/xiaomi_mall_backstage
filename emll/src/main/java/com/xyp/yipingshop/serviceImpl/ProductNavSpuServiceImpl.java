package com.xyp.yipingshop.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyp.yipingshop.pojo.ProductNavSpu;
import com.xyp.yipingshop.mapper.ProductNavSpuMapper;
import com.xyp.yipingshop.service.ProductNavSpuService;
import com.xyp.yipingshop.uitl.ResponseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductNavSpuServiceImpl extends ServiceImpl<ProductNavSpuMapper, ProductNavSpu> implements ProductNavSpuService {
    @Resource
    private ProductNavSpuMapper productNavSpuMapper;
    @Override
    public ResponseResult<ProductNavSpu> findall() {

        return     ResponseResult.getResponseResult(200,10,productNavSpuMapper.selectList(null),200,"ss");
    }
}
