package com.xyp.yipingshop.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyp.yipingshop.pojo.ProductBrand;
import com.xyp.yipingshop.mapper.ProductBrandMapper;
import com.xyp.yipingshop.service.ProductBrandService;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductBrandServiceImpl extends ServiceImpl<ProductBrandMapper, ProductBrand> implements ProductBrandService {

}
