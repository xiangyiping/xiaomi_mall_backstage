package com.xyp.yipingshop.serviceImpl;

import com.xyp.yipingshop.pojo.ProductOption;
import com.xyp.yipingshop.mapper.ProductOptionMapper;
import com.xyp.yipingshop.service.ProductOptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductOptionServiceImpl extends ServiceImpl<ProductOptionMapper, ProductOption> implements ProductOptionService {

}
