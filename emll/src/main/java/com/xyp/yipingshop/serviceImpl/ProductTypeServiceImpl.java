package com.xyp.yipingshop.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyp.yipingshop.pojo.ProductType;
import com.xyp.yipingshop.mapper.ProductTypeMapper;
import com.xyp.yipingshop.service.ProductTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyp123
 * @since 2020-03-19
 */
@Service
public class ProductTypeServiceImpl extends ServiceImpl<ProductTypeMapper, ProductType> implements ProductTypeService {

}
